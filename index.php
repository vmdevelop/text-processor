<?php

use App\Service;

require __DIR__ . '/vendor/autoload.php';

/**
 * Get request parse json etc...
 */
function response(array $response) {
    header('Content-Type: application/json');
    echo json_encode($response);
}
$jsonReq = file_get_contents("php://input");
$data = json_decode($jsonReq, true);
if (json_last_error() != JSON_ERROR_NONE) {
    response(['error' => 'please provide data to process']);
}




/**
 * START
 */
$service = new Service(['env' => 'prod']);
$processedData = $service->processBatch($data);
response($processedData);
