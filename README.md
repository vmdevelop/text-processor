# About

Text processing service with pipelines.

## Usage

- **Step 1**: install/update composer
- **Step 2**: Start server
- **Step 2**: Request `http://localhost` with `json`.

*Example.*
```json
{
  "job_1": {
      "text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!",
      "methods": [
        "stripTags"
      ]
  },
  "job_2": {
      "text": "Другой Текст Для Обработки",
      "methods": [
        "mb_strtolower", "removeSpaces"
      ]
  }
}
```

## Run Tests

```
vendor\bin\phpunit tests\ServiceTest.php
```
