<?php


namespace Tests;


use App\Service;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    /**
     * @dataProvider jobsProvider
     */
    public function testProcess($expected, $job)
    {
        $service = new Service();
        $this->assertSame($expected, $service->processBatch($job)['job']['text']);
    }

    public function jobsProvider()
    {
        $text = 'Привет, мне на <a href="test@test.ru">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!';
        $labels = [
            'Htmlspecialchars' => ['Привет, мне на &lt;a href=&quot;test@test.ru&quot;&gt;test@test.ru&lt;/a&gt; пришло приглашение встретиться, попить кофе с &lt;strong&gt;10%&lt;/strong&gt; содержанием молока за &lt;i&gt;$5&lt;/i&gt;, пойдем вместе!'],
            'RemoveSpaces' => ['Привет,мнена<ahref="test@test.ru">test@test.ru</a>пришлоприглашениевстретиться,попитькофес<strong>10%</strong>содержаниеммолоказа<i>$5</i>,пойдемвместе!'],
            'RemoveSymbols' => ['Привет мне на <a href="testtestru">testtestru<a> пришло приглашение встретиться попить кофе с <strong>10<strong> содержанием молока за <i>5<i> пойдем вместе'],
            'StripTags' => ['Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!'],
            'ToNumber' => ['10'],
            //Simple functions
            'mb_strtolower' => ['привет, мне на <a href="test@test.ru">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!'],
        ];
        foreach ($labels as $method => &$label) {
            $label[] = [
                'job' => [
                    'text' => $text,
                    'methods' => [lcfirst($method)]
                ]
            ];
        }
        return $labels;
    }
}