<?php


namespace App\Pipeline;


use App\Exceptions\UnknownMethod;
use App\Pipeline\Contracts\Pipe;
use App\Pipeline\Contracts\Pipeline;
use App\Pipes\FunctionPipe;

class Builder
{
    /**
     * Provided methods
     *
     * @var mixed
     */
    protected $methods;

    /**
     * Pipes namespace
     *
     * @var string
     */
    protected $namespace = 'App\Pipes';

    /**
     * Allowed simple functions to use as pipe
     *
     * @var array
     */
    private $allowedFunctions = [
        'strtolower', 'strtoupper', 'mb_strtolower', 'mb_strtoupper', 'strrev'
    ];

    /**
     * Cached Pipes
     *
     * @var array
     */
    private $cachedPipes = [];

    /**
     * AbstractBuilder constructor.
     *
     * @param array $methods
     */
    public function __construct(array $methods)
    {
        $this->methods = $methods;
    }

    /**
     * Build pipeline
     *
     * @return Pipeline
     * @throws UnknownMethod
     */
    public function build(): Pipeline
    {
        $pipeline = new SimplePipeline();

        foreach ($this->methods as $method) {
            $pipeline->pipe($this->makePipe($method));
        }

        return $pipeline;
    }

    /**
     * @param string $name
     * @return Pipe
     * @throws UnknownMethod
     */
    private function makePipe(string $name): Pipe
    {
        if (isset($this->cachedPipes[$name])) {
            return $this->cachedPipes[$name];
        }

        $className = $this->namespace . '\\' . ucfirst($name);
        if (class_exists($className)) {
            $obj = new $className();
        }
        if (!class_exists($className) && function_exists($name) && in_array($name, $this->allowedFunctions)) {
            $obj = new FunctionPipe($name);
        }

        if (!isset($obj)) {
            throw new UnknownMethod("Method with name $name is not defined");
        }
        if (!is_callable($obj) && !($obj instanceof Pipe)) {
            throw new UnknownMethod("Method is not a valid Pipe");
        }

        $this->cachedPipes[$name] = $obj;

        return $obj;
    }
}