<?php


namespace App\Pipeline\Contracts;


interface Pipe
{
    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array;
}