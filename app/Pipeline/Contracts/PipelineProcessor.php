<?php


namespace App\Pipeline\Contracts;


interface PipelineProcessor
{
    /**
     * Process pipes
     *
     * @param array $data
     * @param array $pipes
     * @return array
     */
    public function process(array $data, array $pipes): array;
}