<?php


namespace App\Pipeline\Contracts;


interface Pipeline
{
    /**
     * Add stage
     *
     * @param callable $pipe
     * @return Pipeline
     */
    public function pipe(callable $pipe): Pipeline;

    /**
     * Process pipes
     *
     * @param array $data
     * @return mixed
     */
    public function process(array $data): array;
}