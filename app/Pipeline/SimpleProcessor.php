<?php


namespace App\Pipeline;


use App\Pipeline\Contracts\PipelineProcessor;

class SimpleProcessor implements PipelineProcessor
{
    /**
     * Process stages
     *
     * @param array $data
     * @param array $pipes
     * @return array
     */
    public function process(array $data, array $pipes): array
    {
        foreach ($pipes as $pipe) {
            $data = $pipe($data);
        }

        return is_array($data) ? $data : [$data];
    }
}