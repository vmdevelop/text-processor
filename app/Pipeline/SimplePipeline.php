<?php


namespace App\Pipeline;


use App\Pipeline\Contracts\Pipeline;
use App\Pipeline\Contracts\PipelineProcessor;

class SimplePipeline implements Pipeline
{
    /**
     * Pipes array
     *
     * @var array
     */
    private $pipes = [];

    /**
     * Pipeline processor
     *
     * @var PipelineProcessor
     */
    private $processor;

    /**
     * SimplePipeline constructor.
     *
     * @param PipelineProcessor|null $processor
     * @param array $pipes
     */
    public function __construct(PipelineProcessor $processor = null, array $pipes = [])
    {
        $this->processor = $processor ?? new SimpleProcessor();
        $this->pipes = $pipes;
    }

    /**
     * Add stage
     *
     * @param callable $pipe
     * @return Pipeline
     */
    public function pipe(callable $pipe): Pipeline
    {
        $this->pipes[] = $pipe;
        return $this;
    }

    /**
     * Process pipes
     *
     * @param array $data
     * @return mixed
     */
    public function process(array $data): array
    {
        return $this->processor->process($data, $this->pipes);
    }
}