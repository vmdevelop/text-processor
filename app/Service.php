<?php


namespace App;


use App\Pipeline\Builder;
use App\Validator\BatchDataValidator;
use App\Validator\SingleDataValidator;

/**
 * Class Service
 *
 * @package App
 */
class Service
{
    /**
     * Environment
     *
     * @var string
     */
    private $env = 'prod';

    /**
     * Service constructor.
     * @param array $configs
     */
    public function __construct(array $configs = [])
    {
        $this->env = strtolower($configs['env'] ?? 'prod');
    }

    /**
     * Main method.
     * Start text processing
     *
     * @param array $data
     * @return array
     */
    public function process(array $data): array
    {
        return $this->safeCall(function($data) {
            (new SingleDataValidator($data))->validate();

            $methods = $data['methods'];
            $pipelineData = ['text' => $data['text']];
            $pipeline = (new Builder($methods))->build();
            $processedData = $pipeline->process($pipelineData);

            return [
                'text' => $processedData['text']
            ];
        }, $data);
    }

    /**
     * Process batch data
     *
     * @param array $batchData
     * @return array
     */
    public function processBatch(array $batchData): array
    {
        return $this->safeCall(function($batchData) {
            (new BatchDataValidator($batchData))->validate();

            $results = [];
            foreach ($batchData as $jobId => $data) {
                $results[$jobId] = $this->process($data);
            }

            return $results;
        }, $batchData);
    }

    /**
     * Safe call process
     *
     * @param callable $action
     * @param $data
     * @return array
     */
    private function safeCall(callable $action, $data)
    {
        if ($this->env == 'prod') {
            try {
                return $action($data);
            } catch (\Throwable $e) {
                return ['error' => 'Something went wrong'];
            }
        } else {
            return $action($data);
        }
    }
}