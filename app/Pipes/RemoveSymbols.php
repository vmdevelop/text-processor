<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class RemoveSymbols implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = str_replace(['.',',','/','!','@','#','$','%','^','&','*','(',')'], '', $data['text']);

        return $data;
    }
}