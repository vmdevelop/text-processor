<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class FunctionPipe implements Pipe
{
    /**
     * Function name
     *
     * @var callable
     */
    private $function;

    /**
     * FunctionPipe constructor.
     *
     * @param callable $function
     */
    public function __construct(callable $function)
    {
        $this->function = $function;
    }

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = ($this->function)($data['text']);

        return $data;
    }
}