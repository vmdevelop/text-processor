<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class Htmlspecialchars implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = htmlspecialchars($data['text']);

        return $data;
    }
}