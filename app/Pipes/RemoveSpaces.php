<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class RemoveSpaces implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = preg_replace('/\s/','',$data['text']);

        return $data;
    }
}