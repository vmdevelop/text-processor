<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class ReplaceSpacesToEol implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = preg_replace('/\s/', PHP_EOL, $data['text']);

        return $data;
    }
}