<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class ToNumber implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        preg_match('/\d+/', $data['text'], $numsArray);
        if (empty($numsArray)) {
            $data['text'] = null;
        } else {
            $data['text'] = reset($numsArray);
        }

        return $data;
    }
}