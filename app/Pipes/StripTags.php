<?php


namespace App\Pipes;


use App\Pipeline\Contracts\Pipe;

class StripTags implements Pipe
{

    /**
     * Run pipe
     *
     * @param array $data
     * @return array
     */
    public function __invoke(array $data): array
    {
        $data['text'] = strip_tags($data['text']);

        return $data;
    }
}