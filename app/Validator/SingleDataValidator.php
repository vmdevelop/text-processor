<?php


namespace App\Validator;


use App\Exceptions\InvalidData;

class SingleDataValidator extends AbstractValidator
{

    /**
     * Validate data and throw exception on invalid data
     *
     * @throws InvalidData
     */
    public function validate(): void
    {
        if (!isset($this->data['methods'])) {
            throw new InvalidData("key 'methods' is missing from 'job'");
        }

        if (!isset($this->data['text'])) {
            throw new InvalidData("key 'text' is missing from 'job'");
        }
    }
}