<?php


namespace App\Validator;


use App\Exceptions\InvalidData;

class BatchDataValidator extends AbstractValidator
{

    /**
     * Validate data and throw exception on invalid data
     *
     * @throws InvalidData
     */
    public function validate(): void
    {
        array_map(function($data) {
            if (!is_array($data)) {
                throw new InvalidData("Batch data must contain single data arrays");
            }
        }, $this->data);
    }
}