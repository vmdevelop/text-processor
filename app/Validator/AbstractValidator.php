<?php


namespace App\Validator;


use App\Exceptions\InvalidData;

abstract class AbstractValidator
{
    /**
     * Data to validate
     *
     * @var array
     */
    protected $data;

    /**
     * AbstractValidator constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Validate data and throw exception on invalid data
     *
     * @throws InvalidData
     */
    abstract public function validate(): void;
}